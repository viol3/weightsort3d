﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeesawTutorial : LocalSingleton<SeesawTutorial>
{
    public void StartTutorial()
    {
        StartCoroutine(Tutorial());
    }

    IEnumerator Tutorial()
    {
        WeightItemStack stack1 = GameObject.Find("WeightItemStack (3)").GetComponent<WeightItemStack>();
        stack1.SetTutorialPointerActive(true);
        yield return new WaitUntil(() => stack1.IsSelected());
        stack1.SetTutorialPointerActive(false);
        WeightItemStack stack2 = GameObject.Find("WeightItemStack (1)").GetComponent<WeightItemStack>();
        stack2.SetTutorialPointerActive(true);
        yield return new WaitUntil(() => SeesawGameManager.Instance.IsFinished());
        stack2.SetTutorialPointerActive(false);
    }
}
