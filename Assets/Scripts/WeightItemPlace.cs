﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeightItemPlace : MonoBehaviour
{
    [HideInInspector] public WeightItemStack[] Stacks;
    private void Awake()
    {
        Stacks = GetComponentsInChildren<WeightItemStack>();
    }

    public int GetCubeCount()
    {
        int count = 0;
        for (int i = 0; i < Stacks.Length; i++)
        {
            count += Stacks[i].WeightCubes.Count;
        }
        return count;
    }

    public List<WeightCube> GetAllCubes()
    {
        List<WeightCube> result = new List<WeightCube>();
        for (int i = 0; i < Stacks.Length; i++)
        {
            result.AddRange(Stacks[i].WeightCubes);
        }
        return result;
    }
}
