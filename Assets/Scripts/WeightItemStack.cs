﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeightItemStack : MonoBehaviour
{
    public List<WeightCube> WeightCubes;
    

    [SerializeField] private Vector3 _rotateAmount;
    [SerializeField] private Transform _nextTopPoint;
    [SerializeField] private GameObject _tutorialPointer;
    private bool _selected = false;


    void Start()
    {
        OrderWeightCubesInstantly();
        UpdateNextTopPoint();
    }

    void UpdateNextTopPoint()
    {
        float nextY = (SeesawGameManager.Instance.CubeOffset + 0.2f) * (WeightCubes.Count + 1);
        _nextTopPoint.transform.localPosition = new Vector3(_nextTopPoint.transform.localPosition.x, nextY, _nextTopPoint.transform.localPosition.z);
    }

    public void SetTutorialPointerActive(bool value)
    {
        _tutorialPointer.SetActive(value);
    }

    public void OrderWeightCubesInstantly()
    {
        for (int i = 0; i < WeightCubes.Count; i++)
        {
            Transform oldParent = WeightCubes[i].transform.parent;
            WeightCubes[i].transform.SetParent(transform, true);
            WeightCubes[i].transform.localEulerAngles = Vector3.zero;
            WeightCubes[i].transform.localPosition = new Vector3(0, ((SeesawGameManager.Instance.CubeOffset + 0.2f) * (i + 1)) - 3.5f, 0);
            WeightCubes[i].transform.SetParent(oldParent, true);
        }
    }

    public WeightCube GetNextCube()
    {
        if(WeightCubes.Count == 0)
        {
            return null;
        }
        return WeightCubes[WeightCubes.Count - 1];
    }

    public float GetFlyPositionY()
    {
        return (SeesawGameManager.Instance.CubeOffset + 0.2f) * WeightCubes.Count + 4f;
    }

    public float GetNormalPositionY()
    {
        return (SeesawGameManager.Instance.CubeOffset + 0.2f) * WeightCubes.Count;
    }

    public void OnMouseUpAsButton()
    {
        if (SeesawGameManager.Instance.CurrentSeesaw.IsTravelling()
            || SeesawGameManager.Instance.IsFinished())
        {
            return;
        }
        WeightItemStack stack = SeesawGameManager.Instance.CurrentSeesaw.GetSelectedStack();
        if (stack && stack != this)
        {
            SeesawGameManager.Instance.CurrentSeesaw.PutCubeToSelectedStack(this);
        }
        else if(stack == null || stack == this)
        {
            TryToSelect();
        }
        
    }

    public void RemoveCube(WeightCube wc)
    {
        WeightCubes.Remove(wc);
        UpdateNextTopPoint();
    }

    public void AddCube(WeightCube wc)
    {
        WeightCubes.Add(wc);
        UpdateNextTopPoint();
    }

    public void Unselect()
    {
        _selected = false;
    }

    public void TryToSelect()
    {
        if (_selected)
        {
            WeightCube wc = GetNextCube();
            if (wc == null)
            {
                return;
            }
            SeesawGameManager.Instance.CurrentSeesaw.SetSelectedStack(null);
            Unselect();
            wc.transform.DOKill();
            wc.transform.DOLocalMoveY(GetNormalPositionY(), 0.1f);
        }
        else
        {
            WeightCube wc = GetNextCube();
            if (wc == null)
            {
                return;
            }
            SeesawGameManager.Instance.CurrentSeesaw.SetSelectedStack(this);
            _selected = true;
            wc.transform.DOKill();
            wc.transform.DOLocalMoveY(GetFlyPositionY(), 0.1f);
        }
        
    }

    public Vector3 GetRotateAmount()
    {
        return _rotateAmount;
    }

    public bool IsOwned(WeightCube cube)
    {
        return WeightCubes.Contains(cube);
    }

    public bool IsSelected()
    {
        return _selected;
    }

    public Vector3 GetNextPointPosition()
    {
        return _nextTopPoint.position;
    }

    public Vector3 GetNextPointRotation()
    {
        return _nextTopPoint.eulerAngles;
    }
}
