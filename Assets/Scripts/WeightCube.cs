﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeightCube : MonoBehaviour
{
    [SerializeField] private int _colorId = 0;
    [SerializeField] private Renderer _renderer;
    private void Start()
    {
        _renderer.material = SeesawGameManager.Instance.GetColorMaterialByIndex(_colorId);
    }
    private void OnMouseUpAsButton()
    {
        if(SeesawGameManager.Instance.IsFinished())
        {
            return;
        }
        WeightItemStack stack = SeesawGameManager.Instance.GetStackByCube(this);
        if(stack)
        {
            stack.OnMouseUpAsButton();
        }
    }

    public int GetColorId()
    {
        return _colorId;
    }

}
