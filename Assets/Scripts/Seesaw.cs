﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Seesaw : MonoBehaviour
{
    [Header("Unbalanced Animation")]
    [SerializeField] private float _angleDifference = 5f;
    [SerializeField] private float _changeDuration = 0.5f;
    [SerializeField] private float _travelSpeed = 50f;
    [SerializeField] private float _totalVectorMultiplier = 3f;
    [SerializeField] private float _breakMagnitude = 27;
    [SerializeField] private float _breakForceMultiplier = 1f;
    [SerializeField] private float _maxMagnitudeToWin = 0;
    [SerializeField] private int _maxStackedCubeCount = 3;

    [Space]

    public WeightItemPlace[] Places;

    private WeightItemStack _selectedStack = null;
    private Tweener _unbalancedRotateTween;
    private Coroutine _unbalancedAnimCo;
    private bool _travelling = false;
    private Vector3 _totalRotationVector;
    
    
    void Start()
    {
        Places = GetComponentsInChildren<WeightItemPlace>();
        StartUnbalancedAnimation();
    }

    void StartUnbalancedAnimation()
    {
        _unbalancedAnimCo = StartCoroutine(UnbalancedRotateAnim());
    }

    void StopUnbalancedAnimation()
    {
        if(_unbalancedAnimCo != null)
        {
            StopCoroutine(_unbalancedAnimCo);
        }
        if (_unbalancedRotateTween != null)
        {
            _unbalancedRotateTween.Kill();
            _unbalancedRotateTween = null;
        }
    }

    IEnumerator UnbalancedRotateAnim()
    {
        float duration = _changeDuration * GetUnbalancedRatio();
        //if(_totalRotationVector.sqrMagnitude > 5)
        //{
        //    float diff = _maxMagnitudeToWin - _totalRotationVector.sqrMagnitude;
        //    //float multiplier = 5 / _totalRotationVector.sqrMagnitude;
        //    //if(multiplier < 0.2f)
        //    //{
        //    //    multiplier = 0.2f;
        //    //}
        //    duration = diff / 2f;
        //    if(duration < 0.1f)
        //    {
        //        duration = _changeDuration;
        //    }
        //}
        _unbalancedRotateTween = transform.DORotate(Vector3.forward * _angleDifference, duration).SetRelative();
        yield return _unbalancedRotateTween.WaitForCompletion();
        while(true)
        {
            _unbalancedRotateTween = transform.DORotate(Vector3.back * _angleDifference * 2f, duration).SetRelative();
            yield return _unbalancedRotateTween.WaitForCompletion();
            _unbalancedRotateTween = transform.DORotate(Vector3.forward * _angleDifference * 2f, duration).SetRelative();
            yield return _unbalancedRotateTween.WaitForCompletion();
        }
    }

    float GetUnbalancedRatio()
    {
        if(Mathf.Approximately(_totalRotationVector.sqrMagnitude, 0f))
        {
            return 1f;
        }
        if((1f - (_totalRotationVector.sqrMagnitude / _breakMagnitude)) < 0.3f)
        {
            return 0.3f;
        }
        return 1f - _totalRotationVector.sqrMagnitude / _breakMagnitude;
        //return (diff - 1f) / 3f;
    }

    public bool IsCompleted()
    {
        //Check if number of each places is same
        for (int i = 0; i < Places.Length - 1; i++)
        {
            for (int j = 0; j < Places[i].Stacks.Length; j++)
            {
                if (Places[i].Stacks[j].WeightCubes.Count == 0)
                {
                    continue;
                }
                else if (Places[i].Stacks[j].WeightCubes.Count == 1)
                {
                    return false;
                }
            }
            //if(Places[i].GetCubeCount() == 0)
            //{
            //    return false;
            //}
            //if(Places[i].GetCubeCount() != Places[i + 1].GetCubeCount())
            //{
            //    return false;
            //}
        }

        //Check if all places have got 1 color only
        for (int i = 0; i < Places.Length; i++)
        {
            List<WeightCube> cubes = Places[i].GetAllCubes();
            for (int j = 0; j < cubes.Count - 1; j++)
            {
                if(cubes[j].GetColorId() != cubes[j + 1].GetColorId())
                {
                    return false;
                }
            }
        }

        //Same colored cubes should included in only 1 stack
        for (int i = 0; i < Places.Length; i++)
        {
            int stacksContainCube = 0;
            for (int j = 0; j < Places[i].Stacks.Length; j++)
            {
                if (Places[i].Stacks[j].WeightCubes.Count != 0)
                {
                    stacksContainCube++;
                }
            }
            if(stacksContainCube > 1)
            {
                return false;
            }
        }

        if(_totalRotationVector.sqrMagnitude > _maxMagnitudeToWin)
        {
            return false;
        }

        return true;
    }

    public void SetSelectedStack(WeightItemStack stack)
    {
        _selectedStack = stack;
    }

    public bool IsTravelling()
    {
        return _travelling;
    }

    public WeightItemStack GetSelectedStack()
    {
        return _selectedStack;
    }

    public void PutCubeToSelectedStack(WeightItemStack stack)
    {
        if(_travelling)
        {
            return;
        }
        _travelling = true;
        StartCoroutine(TravelCubeToStack(stack));
    }

    IEnumerator TravelCubeToStack(WeightItemStack stack)
    {
        StopUnbalancedAnimation();

        WeightCube wc = _selectedStack.GetNextCube();

        Vector3 nextPos = stack.GetNextPointPosition();
        Vector3 nextRotation = stack.GetNextPointRotation();

        float flyPosY = 40;
        yield return wc.transform.DOMoveY(flyPosY, _travelSpeed).SetSpeedBased().WaitForCompletion();
        yield return wc.transform.DOMove(new Vector3(nextPos.x, wc.transform.position.y, nextPos.z), _travelSpeed).SetSpeedBased().WaitForCompletion();

        stack.AddCube(wc);
        _selectedStack.RemoveCube(wc);

        wc.transform.DORotate(nextRotation, 0.1f);
        yield return wc.transform.DOMoveY(nextPos.y, _travelSpeed / 1.25f).SetSpeedBased().WaitForCompletion();
        stack.OrderWeightCubesInstantly();
        stack.Unselect();
        _selectedStack.Unselect();
        _selectedStack = null;
        CalculateTotalRotationVector();
        yield return transform.DOLocalRotate(_totalRotationVector * _totalVectorMultiplier, 0.3f).WaitForCompletion();
        
        _travelling = false;
        if(IsFailed())
        {
            ThrowCubes();
            SeesawGameManager.Instance.Finish(false);
        }
        else if(IsCompleted())
        {
            SeesawGameManager.Instance.Finish(true);
        }
        else
        {
            StartUnbalancedAnimation();
        }
        
    }

    void CalculateTotalRotationVector()
    {
        Vector3 total = Vector3.zero;
        for (int i = 0; i < Places.Length; i++)
        {
            for (int j = 0; j < Places[i].Stacks.Length; j++)
            {
                total += Places[i].Stacks[j].GetRotateAmount() * Places[i].Stacks[j].WeightCubes.Count;
            }
        }
        _totalRotationVector = total;
    }

    bool IsFailed()
    {
        Debug.Log("Magnitude : " + _totalRotationVector.sqrMagnitude);
        if(_totalRotationVector.sqrMagnitude > _breakMagnitude)
        {
            return true;
        }
        else
        {
            for (int i = 0; i < Places.Length; i++)
            {
                for (int j = 0; j < Places[i].Stacks.Length; j++)
                {
                    if(Places[i].Stacks[j].WeightCubes.Count > _maxStackedCubeCount)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    void ThrowCubes()
    {
        for (int i = 0; i < Places.Length; i++)
        {
            List<WeightCube> cubes = Places[i].GetAllCubes();
            for (int j = 0; j < cubes.Count; j++)
            {
                cubes[j].gameObject.GetComponent<BoxCollider>().material = SeesawGameManager.Instance.GetCubePhysicMaterial();
                Rigidbody rigid = cubes[j].gameObject.AddComponent<Rigidbody>();
                rigid.AddForce(new Vector3(-_totalRotationVector.z, 0f, _totalRotationVector.x) * 3f * _breakForceMultiplier, ForceMode.Impulse);
            }
        }
    }
}
