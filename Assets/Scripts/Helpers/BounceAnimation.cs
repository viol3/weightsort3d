﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BounceAnimation : MonoBehaviour
{
    [SerializeField] private float _offsetY;
    [SerializeField] private float _duration;
    void Start()
    {
        transform.DOLocalMoveY(_offsetY, _duration).SetRelative().SetEase(Ease.InSine).SetLoops(-1, LoopType.Yoyo);
    }

}
