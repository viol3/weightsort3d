﻿
using System.Collections;
using UnityEngine;

public class SeesawGameManager : LocalSingleton<SeesawGameManager>
{
    [SerializeField] private WeightGameSkin _skin;
    [SerializeField] private PhysicMaterial _cubePhysicMaterial;
    [SerializeField] private PuzzleEndPanel _puzzleEndPanel;
    [SerializeField] private GameObject[] _confettiGOs;

    public float CubeOffset = 5;
    public Seesaw CurrentSeesaw;

    private bool _finished = false;

    IEnumerator Start()
    {
        _puzzleEndPanel.OnClick += PuzzleEndPanel_OnClick;
        
        HCLevelManager.Instance.Init();
        HCLevelManager.Instance.GenerateCurrentLevel();
        if(HCLevelManager.Instance.GetGlobalLevelIndex() == 0)
        {
            SeesawTutorial.Instance.StartTutorial();
        }
        ElephantSDK.Elephant.LevelStarted(HCLevelManager.Instance.GetGlobalLevelIndex());
        _puzzleEndPanel.SetLevel(HCLevelManager.Instance.GetGlobalLevelIndex() + 1);
        CurrentSeesaw = HCLevelManager.Instance.GetCurrentLevel().GetComponent<Seesaw>();
        yield return FaderManager.Instance.OpenTheater();
        
    }

    private void PuzzleEndPanel_OnClick()
    {
        SceneLoader.Instance.LoadScene("Main");
    }

    public Material GetColorMaterialByIndex(int index)
    {
        return _skin.CubeMaterials[index];
    }

    public PhysicMaterial GetCubePhysicMaterial()
    {
        return _cubePhysicMaterial;
    }

    public WeightItemStack GetStackByCube(WeightCube wc)
    {
        for (int i = 0; i < CurrentSeesaw.Places.Length; i++)
        {
            for (int j = 0; j < CurrentSeesaw.Places[i].Stacks.Length; j++)
            {
                if(CurrentSeesaw.Places[i].Stacks[j].IsOwned(wc))
                {
                    return CurrentSeesaw.Places[i].Stacks[j];
                }
            }
        }
        return null;
    }

    public void Finish(bool win)
    {
        if(_finished)
        {
            return;
        }
        _finished = true;
        if (win)
        {
            ElephantSDK.Elephant.LevelCompleted(HCLevelManager.Instance.GetGlobalLevelIndex());
            for (int i = 0; i < _confettiGOs.Length; i++)
            {
                _confettiGOs[i].SetActive(true);
            }
            HCLevelManager.Instance.LevelUp();
            StartCoroutine(Win());
        }
        else
        {
            ElephantSDK.Elephant.LevelFailed(HCLevelManager.Instance.GetGlobalLevelIndex());
            StartCoroutine(Lose());
        }
    }


    public bool IsFinished()
    {
        return _finished;
    }

    IEnumerator Lose()
    {
        yield return new WaitForSeconds(1f);
        _puzzleEndPanel.Show(false);
    }

    IEnumerator Win()
    {
        yield return new WaitForSeconds(1f);
        _puzzleEndPanel.Show(true);
    }


}
