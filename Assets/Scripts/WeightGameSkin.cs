﻿using UnityEngine;

[CreateAssetMenu(fileName = "WeightSkin", menuName = "ScriptableObjects/WeightSkin", order = 1)]
public class WeightGameSkin : ScriptableObject
{
    public Material[] CubeMaterials;
}